import csv
import os
import json
import zipfile

from unittest import mock
from worker.tasks import create_dump, redis_client
from worker.settings import DUMP_DIR


class TestCreateDump:

    def teardown(self):

        for root, dirs, files in os.walk(DUMP_DIR):
            for f in files:
                os.unlink(os.path.join(root, f))

        redis_client.flushall()

    @staticmethod
    def _insert_logs():
        logs = [
            ('value:hash1:123457.11', '{"param1": 1, "param2": 2}'),
            ('value:hash2:123457.12', '{"param1": 1, "param2": 3}')
        ]
        for l in logs:
            redis_client.set(l[0], l[1])

        return logs

    def test_no_data(self):
        result = create_dump.apply()
        assert result.result == 0

    def test_create_dump(self):

        logs = self._insert_logs()

        result = create_dump.apply()
        assert result.result == 2

        keys = redis_client.keys('value:*')
        assert keys == []

        files = os.listdir(DUMP_DIR)
        assert len(files) == 1
        assert files[0].endswith('.csv.zip')
        assert files[0].startswith('dump_')

        with zipfile.ZipFile(os.path.join(DUMP_DIR, files[0])) as zip_dump:
            zip_dump.extractall()
            zip_dump.close()

        with open(os.path.join(DUMP_DIR, files[0].replace('.zip', ''))) as csv_dump:

            csv_reader = csv.reader(csv_dump)
            data = list(csv_reader)
            assert len(data) == 3
            assert data[0] == ['key', 'value']

            for i, log in enumerate(logs):
                assert data[i+1][0] == logs[i][0]
                assert json.loads(data[i+1][1]) == json.loads(logs[i][1])

        assert result.state == 'SUCCESS'

    @mock.patch('worker.tasks.CreateDumpTask._run')
    def test_retry(self, run_mock):

        self._insert_logs()

        run_mock.side_effect = Exception('some exception')
        result = create_dump.apply()
        assert run_mock.call_count == 4
        assert result.state == 'FAILURE'

        run_mock.side_effect = [Exception('some exception'), 5]
        result = create_dump.apply()
        assert result.state == 'RETRY'
