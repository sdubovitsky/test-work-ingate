import os

REDIS_HOST = os.getenv("REDIS_HOST")
REDIS_PORT = os.getenv("REDIS_PORT")
REDIS_DB = os.getenv("REDIS_DB")

BROKER_URL = f'redis://{REDIS_HOST}:{REDIS_PORT}/{REDIS_DB}'

DUMP_INTERVAL = int(os.getenv('DUMP_INTERVAL', 30))

DUMP_DIR = '/dump'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': u'%(asctime)s:%(levelname)s:%(name)s.%(funcName)s: %(message)s',
        },
    },
    'handlers': {
        'basic': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
            'level': 'DEBUG',
        },
    },
    'loggers': {
        'root': {
            'handlers': ['basic'],
            'level': 'DEBUG',
        },
        'celery': {
            'handlers': ['basic'],
            'level': 'DEBUG',
        },
        'worker': {
            'handlers': ['basic'],
            'level': 'DEBUG',
            'propagate': True
        },
    }
}
