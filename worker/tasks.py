import csv
import logging
import time
import redis
import zipfile

from io import StringIO

from worker.celery import app
from worker.settings import REDIS_HOST, REDIS_PORT, REDIS_DB, DUMP_DIR


logger = logging.getLogger(__name__)

redis_client = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)


class CreateDumpTask(app.Task):

    def __init__(self):
        self.max_retries = 3
        self.time_limit = 30
        self.default_retry_delay = 10

    @staticmethod
    def _get_values(keys) -> list:
        return redis_client.mget(*keys)

    @staticmethod
    def _clear_db(keys) -> None:
        redis_client.delete(*keys)

    def _prepare_csv_data(self, keys) -> list:
        logger.info(f'Keys: {keys}')
        data = zip(
            [key.decode() for key in keys],
            [val.decode() for val in self._get_values(keys)]
        )
        # сортируем по timestamp
        csv_data = sorted(data, key=lambda tup: tup[0].split(':')[2])

        return csv_data

    def _create_dump(self, dump_file, keys) -> None:
        with zipfile.ZipFile(f'{dump_file}.zip', 'w', zipfile.ZIP_DEFLATED) as zip_file:
            string_buffer = StringIO()
            writer = csv.writer(string_buffer)
            writer.writerow(['key', 'value'])
            writer.writerows(self._prepare_csv_data(keys))
            zip_file.writestr(dump_file, string_buffer.getvalue())

    def _run(self):
        logger.info(f'Start create dump task, request: {self.request}')

        keys = redis_client.keys('value:*')
        dump_file = f'{DUMP_DIR}/dump_{time.time()}.csv'

        if not keys:
            logger.info(f'No data, skip dump creation')
            return 0

        self._create_dump(dump_file, keys)
        self._clear_db(keys)

        logger.info(f'Dump: {dump_file} created, log count: {len(keys)} ')

        time.sleep(5)  # for demo only

        return len(keys)

    def run(self):
        try:
            return self._run()
        except Exception as e:
            logger.exception('Got unhandled exception')
            self.retry()


create_dump = app.register_task(CreateDumpTask())
