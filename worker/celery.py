import logging.config

from celery import Celery
from kombu import Exchange, Queue

from worker.settings import BROKER_URL, LOGGING, DUMP_INTERVAL


logging.config.dictConfig(LOGGING)
logger = logging.getLogger('worker')

app = Celery('worker', broker=BROKER_URL, include=['tasks'])

app.conf.task_queues = (
    Queue('default_queue', Exchange('default_exchange'), routing_key='default_key'),
)

app.conf.task_default_queue = 'default_queue'
app.conf.task_default_exchange = 'default_exchange'
app.conf.task_default_routing_key = 'default_key'

app.conf.task_routes = {
    '*': {'queue': 'default_queue', 'routing_key': 'default_key'},
}

app.conf.beat_schedule = {
    'create_dump': {
        'task': 'tasks.CreateDumpTask',
        'schedule': DUMP_INTERVAL
    }
}

app.conf.timezone = 'UTC'
