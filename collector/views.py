import logging

import aiohttp_jinja2
from aiohttp import web

from collector.log import Log
from collector.settings import DUMP_INTERVAL

logger = logging.getLogger(__name__)


async def health_check(request: web.Request) -> web.Response:
    return web.json_response({'health': 'ok'})


async def create_log(request: web.Request) -> web.Response:
    data = dict(await request.post())
    logger.debug(f'Log received : {data}')

    log = Log(data)
    log_hash = await request.app['collector'].save(log)

    return web.json_response({'hash': log_hash}, status=201)


@aiohttp_jinja2.template('statistic.jinja2')
async def get_status(request: web.Request) -> dict:
    count_keys = await request.app['redis'].keys('count:*')
    statistic = []

    if count_keys:
        count_values = await request.app['redis'].mget(*count_keys)
        data = zip(
            [key.decode().replace('count:', '') for key in count_keys],
            [val.decode() for val in count_values]
        )
        # сортировка по значению
        statistic = sorted(data, key=lambda tup: tup[1], reverse=True)

    tasks_resp = await request.app['worker_client'].get_tasks()
    last_task = list(tasks_resp.values())[0] if tasks_resp else {}

    return {'statistic': statistic, 'last_task': last_task, 'dump_interval': DUMP_INTERVAL}
