import logging.config
import os

import aiohttp_jinja2
import aioredis
import jinja2

from aiohttp import web, ClientSession

from collector.log import Collector
from collector.settings import LOGGING, REDIS_HOST, REDIS_DB, TEMPLATE_DIR
from collector.routes import setup_routes
from collector.utils import WorkerClient, get_date_timestamp


logging.config.dictConfig(LOGGING)
logger = logging.getLogger('collector')


async def init_redis(app: web.Application) -> None:
    app['redis'] = await aioredis.create_redis_pool(REDIS_HOST, db=REDIS_DB)


async def close_redis(app: web.Application) -> None:
    app['redis'].close()
    await app['redis'].wait_closed()


async def init_http_client(app: web.Application) -> None:
    app['http_client'] = ClientSession()


async def close_http_client(app: web.Application) -> None:
    await app['http_client'].close()


async def init_worker_client(app: web.Application) -> None:
    app['worker_client'] = WorkerClient(
        app['http_client'],
        os.getenv('WORKER_URL')
    )


async def init_collector(app: web.Application) -> None:
    app['collector'] = Collector(app['redis'])


def setup_jinja(app: web.Application) -> None:
    jinja = aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(TEMPLATE_DIR))
    jinja.filters['get_datetime'] = get_date_timestamp


def init_app() -> web.Application:
    app = web.Application()

    setup_jinja(app)
    setup_routes(app)

    app.on_startup.extend([
        init_redis,
        init_http_client,
        init_worker_client,
        init_collector
    ])

    app.on_shutdown.extend([
        close_redis,
        close_http_client
    ])

    return app


if __name__ == '__main__':
    logger.info('Starting app')
    web.run_app(init_app(), port=8000)
