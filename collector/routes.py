from aiohttp import web

from collector.views import health_check, create_log, get_status


def setup_routes(app):
    app.add_routes([
        web.get('/api/health', health_check),
        web.post('/api/log', create_log),
        web.get('/status', get_status)
    ])
