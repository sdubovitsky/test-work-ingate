import json
import hashlib
import logging

import time

from aioredis import Redis
from aioredis.commands.transaction import MultiExec


logger = logging.getLogger(__name__)


class Log:

    def __init__(self, data: dict) -> None:
        self.data = data

    @staticmethod
    def get_hash(log_json: str) -> str:
        return hashlib.md5(log_json.encode('utf-8')).hexdigest()

    def get_json(self) -> str:
        return json.dumps(dict(self.data), sort_keys=True)


class Collector:

    def __init__(self, redis: Redis):
        self.redis = redis

    @staticmethod
    def _insert_log(tr: MultiExec, log_hash: str, log_json: str) -> None:
        logger.debug('Saving log')
        value_key = f'value:{log_hash}:{time.time()}'
        tr.set(value_key, log_json)

    @staticmethod
    def _increase_count(tr: MultiExec, log_hash: str) -> None:
        logger.debug('Increasing counter log')
        tr.incr(f'count:{log_hash}')

    async def save(self, log: Log) -> str:
        transaction = self.redis.multi_exec()

        log_json = log.get_json()
        log_hash = log.get_hash(log_json)

        self._insert_log(transaction, log_hash, log_json)
        self._increase_count(transaction, log_hash)

        await transaction.execute()
        logger.debug('Log save done')

        return log_hash
