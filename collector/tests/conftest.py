import pytest

from collector.app import init_app


async def clear_test_redis(app):
    await app['redis'].flushall()


def init_test_app():
    app = init_app()
    app.on_shutdown.insert(0, clear_test_redis)
    return app


@pytest.fixture
def app(loop):
    return init_test_app()


@pytest.fixture
async def client(aiohttp_client, app):
    return await aiohttp_client(app)


class HttpResp:
    def __init__(self, status=200, json_resp=None):
        self.status = status
        self.json_resp = json_resp or {}

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc, tb):
        pass

    async def json(self):
        return self.json_resp


WORKER_RESPONSE = {
    "task_uuid": {
        "uuid": "task_uuid",
        "name": "tasks.create_dump",
        "state": "SUCCESS",
        "received": 1551605446.4541628,
        "sent": None,
        "started": 1551605446.456893,
        "rejected": None,
        "succeeded": 1551605461.4341753,
        "failed": None,
        "retried": None,
        "revoked": None,
        "args": None,
        "kwargs": "{}",
        "eta": None,
        "expires": None,
        "retries": 0,
        "result": "32",
        "exception": None,
        "timestamp": 1551605461.4341753,
        "runtime": 15.012376899947412,
        "traceback": None,
        "exchange": None,
        "routing_key": None,
        "clock": 9485,
        "client": None,
        "root": "1d65f053-01a9-4803-a73b-08dc3ee28120",
        "root_id": "1d65f053-01a9-4803-a73b-08dc3ee28120",
        "parent": None,
        "parent_id": None,
        "children": []
    }
}
