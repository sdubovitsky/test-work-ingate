import json
import asynctest

from collector.settings import DUMP_INTERVAL
from collector.tests.conftest import HttpResp, WORKER_RESPONSE
from collector.utils import get_date_timestamp


async def test_health(client):
    resp = await client.get('/api/health')
    assert resp.status == 200
    resp_json = await resp.json()
    assert resp_json == {'health': 'ok'}


async def test_create_log(client):
    log_1_data = {'param_1':  '1', 'param_2': '2'}
    resp = await client.post('/api/log', data=log_1_data)
    assert resp.status == 201
    resp_json = await resp.json()
    log_1_hash = resp_json['hash']
    assert log_1_hash

    # параметры лога в другом порядке
    log_2_data = {'param_2': '2', 'param_1': '1'}
    resp = await client.post('/api/log', data=log_2_data)
    assert resp.status == 201
    resp_json = await resp.json()
    log_2_hash = resp_json['hash']
    assert log_1_hash == log_2_hash

    # проверка ключей счетчиков
    count_keys = await client.app['redis'].keys('count:*')
    assert len(count_keys) == 1
    assert count_keys[0].decode().replace('count:', '') == log_1_hash == log_2_hash

    # проверка значения счетчика
    count = await client.app['redis'].get(count_keys[0])
    assert count == b'2'

    # проверка ключей данных
    value_keys = await client.app['redis'].keys('value:*')
    assert len(value_keys) == 2
    keys = [v_key.decode().split(':') for v_key in value_keys]
    assert keys[0][1] == log_1_hash == log_2_hash
    assert get_date_timestamp(float(keys[0][2]))

    # проверка данных
    values = await client.app['redis'].mget(*value_keys)
    assert len(values) == 2
    values = [json.loads(val.decode()) for val in values]
    assert values[0] == values[1] == log_1_data == log_2_data

    # третий лог
    log_3_data = {'param_2': '2', 'param_1': '1', 'param_3': '3'}
    resp = await client.post('/api/log', data=log_3_data)
    resp_json = await resp.json()
    log_3_hash = resp_json['hash']
    assert log_3_hash != log_1_hash

    count = await client.app['redis'].get(f'count:{log_3_hash}')
    assert count == b'1'

    value_keys = await client.app['redis'].keys('value:*')
    assert len(value_keys) == 3


@asynctest.patch('collector.app.ClientSession.get')
async def test_status(worker_mock, client):
    logs_data = [
        {'param_1': '1', 'param_2': '2'},
        {'param_1': '1', 'param_3': '3'}
    ]

    hashes = []

    for log in logs_data:
        resp = await client.post('/api/log', data=log)
        assert resp.status == 201
        resp_json = await resp.json()
        hashes.append(resp_json['hash'])

    # нет данных о периодических задачах
    worker_mock.return_value = HttpResp(json_resp={})

    resp = await client.get('/status')
    assert resp.status == 200
    resp_data = await resp.read()
    resp_data = resp_data.decode()
    assert '<!DOCTYPE html>' in resp_data
    assert '<title>Status</title>' in resp_data
    assert 'unknown status' in resp_data

    for h in hashes:
        assert f'<td>{h}</td>' in resp_data
        assert f'<td>1</td>' in resp_data

    # есть данные о задачах
    worker_mock.return_value = HttpResp(json_resp=WORKER_RESPONSE)

    resp = await client.get('/status')
    assert resp.status == 200
    resp_data = await resp.read()
    resp_data = resp_data.decode()

    assert 'unknown status' not in resp_data
    assert 'in progress' not in resp_data
    assert get_date_timestamp(WORKER_RESPONSE['task_uuid']['succeeded']) in resp_data
    assert WORKER_RESPONSE['task_uuid']['state'] in resp_data
    assert WORKER_RESPONSE['task_uuid']['result'] in resp_data
    assert get_date_timestamp(WORKER_RESPONSE['task_uuid']['received'] + DUMP_INTERVAL) in resp_data

