import datetime
import logging

from typing import Tuple, Union

from aiohttp import ClientSession


logger = logging.getLogger(__name__)


class WorkerClient:

    def __init__(self, http_client: ClientSession, worker_url: str) -> None:
        self.http_client = http_client
        self.worker_url = worker_url

    async def get_tasks(self, limit: int = 1) -> dict:
        url = f'{self.worker_url}/api/tasks'
        data, _ = await _get(self.http_client, url, params={'limit': limit})
        return data


async def _get(http_client: ClientSession, url: str, params: dict = None) -> Tuple[dict, int]:
    logger.debug(f'Getting url: {url}, with params: {params}')
    async with http_client.get(url, params=params) as response:
        logger.debug(f'Got status: {response.status}')
        data = await response.json()
        logger.debug(f'Result: {data}')
        return data, response.status


def get_date_timestamp(ts: float) -> Union[str, None]:
    return datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S') if ts else None
