import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates')

REDIS_HOST = f'redis://{os.getenv("REDIS_HOST")}:{os.getenv("REDIS_PORT")}'

REDIS_DB = int(os.getenv("REDIS_DB", 0))

DUMP_INTERVAL = int(os.getenv("DUMP_INTERVAL", 30))

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': u'%(asctime)s:%(levelname)s:%(name)s.%(funcName)s: %(message)s',
        },
    },
    'handlers': {
        'basic': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
            'level': 'DEBUG',
        },
    },
    'loggers': {
        'root': {
            'handlers': ['basic'],
            'level': 'DEBUG',
        },
        'aiohttp': {
            'handlers': ['basic'],
            'level': 'DEBUG',
        },
        'collector': {
            'handlers': ['basic'],
            'level': 'DEBUG',
            'propagate': True
        },
    }
}
